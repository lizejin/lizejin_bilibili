package com.lizejin.bilibili.domain;

public class JsonResponse<T> {

//    状态码
    private String code;
//返回的提示信息
    private String msg;
//返回的数据
    private T data;

//    不同类型的构造器，以便响应不同应用场景的数据各式需求

    public JsonResponse(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResponse(T data) {
        this.data = data;
        msg = "成功";
        code = "0";
    }
//    直接返回成功状态，不需要返回数据
    public static JsonResponse<String> success(){
        return new JsonResponse<>(null);
    }
//返回成功状态，并返回数据
    public static JsonResponse<String> success(String data){
        return new JsonResponse<>(data);
    }
//直接返回失败状态
    public static JsonResponse<String> fail(){
        return new JsonResponse<>("1","失败");
    }
//    返回特定的失败状态与信息，这个JsonResponse对象处理“0”表示成功，其他状态码为失败
    public static JsonResponse<String> fail(String code,String msg){
        return new JsonResponse<>(code,msg);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JsonResponse{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}

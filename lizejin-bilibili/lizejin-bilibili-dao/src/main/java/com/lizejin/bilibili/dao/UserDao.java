package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.User;
import com.lizejin.bilibili.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface UserDao {

    User selectByPhone(String phone);

    Integer addNewUser(User newUser);

    Integer addUserInfo(UserInfo userInfo);

    User getUserById(Long Id);

    UserInfo getUserInfoByUserId(Long userId);

    Integer updateUserInfo(UserInfo userInfo);

    List<UserInfo> getUserInfoByUserIds(Set<Long> userIds);

    Integer getCountByNick(String nick);

    List<UserInfo> getUserInfoByPage(Map map);

    List<UserInfo> batchGetUserInfoByUserIds(Set<Long> userIdList);
}

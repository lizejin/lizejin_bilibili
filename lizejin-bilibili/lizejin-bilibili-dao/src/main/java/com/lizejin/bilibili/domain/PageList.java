package com.lizejin.bilibili.domain;

import lombok.Data;

import java.util.List;

@Data
public class PageList<T> {

    private Integer total;

    private List<T> list;

    public PageList(Integer total, List<T> list) {
        this.total = total;
        this.list = list;
    }

    public PageList() {
    }
}

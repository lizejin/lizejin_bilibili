package com.lizejin.bilibili.domain.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Component
public @interface APILimitedRole {//编写了一个注解，注解里有个属性 角色列表数组，默认值为空

    String[] limitedRoleCodeLists() default {};
}

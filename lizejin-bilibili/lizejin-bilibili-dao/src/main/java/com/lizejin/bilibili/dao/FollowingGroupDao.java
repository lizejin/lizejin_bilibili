package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.FollowingGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FollowingGroupDao {

    FollowingGroup getById(Long id);

    FollowingGroup getByType(String type);


    List<FollowingGroup> getGroupsByUserId(Long userId);
}

package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.FollowingGroup;
import com.lizejin.bilibili.domain.UserFollowing;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserFollowingDao {

    void deleteByUserIdAndFollowingId(@Param("userId") Long userId, @Param("followingId") Long followingId);

    void addUserFollowing(UserFollowing userFollowing);

    List<UserFollowing> getUserFollowingByUserId(Long userId);

    List<UserFollowing> getUserFansByUserId(Long followingId);

    void addUserFollowingGroup(FollowingGroup group);

     int getUserFollowingGroupByName(String name);

    List<FollowingGroup> getUserFollowingGroupByUserId(Long userId);

    UserFollowing checkFollowing(UserFollowing userFollowing);

    void cancleFollowing(UserFollowing userFollowing);
}

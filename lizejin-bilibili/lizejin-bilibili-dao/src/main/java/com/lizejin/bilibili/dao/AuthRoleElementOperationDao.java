package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.auth.AuthRoleElementOperation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public interface AuthRoleElementOperationDao {

    List<AuthRoleElementOperation> getAuthRoleElementOperationsByRoleIds(@Param("roleIds") Set<Long> roleIds);
}

package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.File;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface FileDao {
    //根据MD5获取文件
    File getFileByMD5(String md5);
    //添加文件
    void addFile(File dbFileMD5);
}

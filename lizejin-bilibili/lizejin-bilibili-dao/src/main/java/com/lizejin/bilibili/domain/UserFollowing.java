package com.lizejin.bilibili.domain;

import lombok.Data;

import java.util.Date;

@Data
public class UserFollowing {

    private Long id;

    private Long userId;

    private Long followingId;

    private Long groupId;

    private Date createTime;
    //关注的up主的信息【一个】
    private UserInfo userInfo;
}

package com.lizejin.bilibili.domain;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FollowingGroup {

    private Long id;

    private Long userId;

    private String name;

    private String type;

    private Date createTime;

    private Date updateTime;
    //不同分组下的关注的up主的信息【多个】
    private List<UserInfo> followingUserInfoList;

}

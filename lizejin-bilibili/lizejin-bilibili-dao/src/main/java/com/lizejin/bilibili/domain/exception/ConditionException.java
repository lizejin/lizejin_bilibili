package com.lizejin.bilibili.domain.exception;

public class ConditionException extends RuntimeException{

    private static final long serialVersionUid = 1L;

    private String code;
    //自定义异常需要指定相关的状态码和数据
    public ConditionException(String code,String name){
         super(name);
         this.code = code;
    }
    //一般异常直接500
    public ConditionException(String name){
        super(name);
        this.code = "500";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

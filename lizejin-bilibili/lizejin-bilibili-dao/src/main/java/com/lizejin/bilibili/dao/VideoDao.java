package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VideoDao {
    /**
     * @Author @lizejin
     * @Description //视频添加
     * @Date 12:34 2023/4/16
     * @param video 视频信息
     **/
    Integer addVideos(Video video);
    /**
     * @Author @lizejin
     * @Description //批量添加视频标签
     * @Date 12:34 2023/4/16
     * @param videoTagList 标签列表
     **/
    Integer batchAddVideoTags(List<VideoTag> videoTagList);

    /**
     * @Author @lizejin
     * @Description //指定分区的视频总数
     * @Date 12:42 2023/4/16
     * @param params 分区值
     **/
    Integer pageCountVideos(Map<String, Object> params);
    /**
     * @Author @lizejin
     * @Description //指定分区的视频分页查询
     * @Date 12:46 2023/4/16
     * @param  params 分区值，分页条件
     **/
    List<Video> pageListVideos(Map<String, Object> params);
    /**
     * @Author @lizejin
     * @Description //指定视频id查找视频
     * @Date 12:47 2023/4/16
     * @param  id 视频id
     **/
    Video getVideoById(Long id);
    /**
     * @Author @lizejin
     * @Description //查找指定用户的id是否点赞过指定视频id的数据记录，即查找是否点赞过
     * @Date 12:47 2023/4/16
     * @param  videoId,userId 视频id和用户ID
     **/
    VideoLike getVideoLikeByVideoIdAndUserId(@Param("videoId") Long videoId, @Param("userId") Long userId);
    /**
     * @Author @lizejin
     * @Description //给指定用户在指定视频下添加点赞记录，即给视频点赞
     * @Date 12:49 2023/4/16
     * @param  videoLike 点赞的数据信息
     **/
    Integer addVideoLike(VideoLike videoLike);
    /**
     * @Author @lizejin
     * @Description //给指定用户在指定视频下删除点赞记录，即取消点赞
     * @Date 12:49 2023/4/16
     * @param  videoId,userId 视频id和用户ID
     **/
    Integer deleteVideoLike(@Param("videoId") Long videoId,
                            @Param("userId") Long userId);
    /**
     * @Author @lizejin
     * @Description //获取本条视频id的点赞记录数，即计算点赞数
     * @Date 12:50 2023/4/16
     * @param  videoId 视频id
     **/
    Long getVideoLikes(Long videoId);
    /**
     * @Author @lizejin
     * @Description //取消指定用户id下指定视频id的收藏记录，即取消这个用户对本条视频的收藏
     * @Date 12:50 2023/4/16
     * @param  videoId,userId 视频id和用户ID
     **/
    
    Integer deleteVideoCollection(@Param("videoId") Long videoId,
                                  @Param("userId") Long userId);
    /**
     * @Author @lizejin
     * @Description //为指定用户id下指定视频id的增添收藏记录
     * @Date 12:51 2023/4/16
     * @param  videoCollection 当前收藏信息
     **/
    
    Integer addVideoCollection(VideoCollection videoCollection);

    /**
     * @Author @lizejin
     * @Description //获取本条视频id下的收藏记录数
     * @Date 12:33 2023/4/16
     * @param videoId 视频id
     **/
    Long getVideoCollections(Long videoId);

   /**
    * @Author @lizejin
    * @Description //查找当前用户是否收藏过该视频
    * @Date 12:32 2023/4/16
    * @param videoId,userId 视频id和用户id
    **/
    VideoCollection getVideoCollectionByVideoIdAndUserId(@Param("videoId") Long videoId,
                                                         @Param("userId") Long userId);
    /**
     * @Author @lizejin
     * @Description //查找当前用户是否给当前视频投过币
     * @Date 12:52 2023/4/16
     * @param  videoId,userId 视频id和用户id
     **/
    
    VideoCoin getVideoCoinByVideoIdAndUserId(@Param("videoId") Long videoId,
                                             @Param("userId") Long userId);
    /**
     * @Author @lizejin
     * @Description //给视频投币
     * @Date 12:53 2023/4/16
     * @param videoCoin 投币信息
     **/
    
    Integer addVideoCoin(VideoCoin videoCoin);
    /**
     * @Author @lizejin
     * @Description //给视频在此投币，即对当前用户的投币信息的更新
     * @Date 12:54 2023/4/16
     * @param  videoCoin 更新用户的投币信息
     **/
    Integer updateVideoCoin(VideoCoin videoCoin);
    /**
     * @Author @lizejin
     * @Description //计算当前视频的投币数量
     * @Date 12:55 2023/4/16
     * @param videoId 视频id
     **/
    Long getVideoCoinsAmount(Long videoId);
    /**
     * @Author @lizejin
     * @Description //添加视频评论
     * @Date 12:56 2023/4/16
     * @param  videoComment xx
     **/
    
    Integer addVideoComment(VideoComment videoComment);
    /**
     * @Author @lizejin
     * @Description //计算评论总数
     * @Date 12:57 2023/4/16
     * @param  params xx
     **/
    
    Integer pageCountVideoComments(Map<String, Object> params);
    /**
     * @Author @lizejin
     * @Description //分页查询评论
     * @Date 12:57 2023/4/16
     * @param  params xx
     **/
    
    List<VideoComment> pageListVideoComments(Map<String, Object> params);
    /**
     * @Author @lizejin
     * @Description //批量获取根评论
     * @Date 12:57 2023/4/16
     * @param  rootIdList xx
     **/
    
    List<VideoComment> batchGetVideoCommentsByRootIds(@Param("rootIdList") List<Long> rootIdList);
    /**
     * @Author @lizejin
     * @Description //获取视频详情
     * @Date 12:57 2023/4/16
     * @param  videoId 视频id
     **/
    Video getVideoDetails(Long videoId);

//    Integer addVideoView(VideoView videoView);
//
//    Integer getVideoViewCounts(Long videoId);
//
//    VideoView getVideoView(Map<String, Object> params);
//
//    List<UserPreference> getAllUserPreference();
//
//    List<Video> batchGetVideosByIds(@Param("idList") List<Long> idList);
//
//    Integer batchAddVideoBinaryPictures(@Param("pictureList") List<VideoBinaryPicture> pictureList);
   /**
    * @Author @lizejin
    * @Description //获取当前视频的标签详细内容
    * @Date 12:57 2023/4/16
    * @param  videoId 视频id
    **/
   
    List<VideoTag> getVideoTagsByVideoId(Long videoId);
     /**
      * @Author @lizejin
      * @Description //删除当前视频下的某些标签信息
      * @Date 12:59 2023/4/16
      * @param  tagIdList，videoId 标签列表【id】，视频id
      **/
     
    Integer deleteVideoTags(@Param("tagIdList") List<Long> tagIdList,
                            @Param("videoId") Long videoId);

//    List<VideoBinaryPicture> getVideoBinaryImages(Map<String, Object> params);
}

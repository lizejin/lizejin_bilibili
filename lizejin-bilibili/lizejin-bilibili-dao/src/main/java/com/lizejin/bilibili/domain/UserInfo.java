package com.lizejin.bilibili.domain;

import lombok.Data;

import java.util.Date;

@Data
public class UserInfo {

    private Long id;

    private Long userId;

    private String nick;

    private String avatar;

    private String sign;

    private String gender;

    private String birth;

    private Date createTime;

    private Date updateTime;
    //是否关注 TRUE为关注 false为未关注
    private boolean followed;

}

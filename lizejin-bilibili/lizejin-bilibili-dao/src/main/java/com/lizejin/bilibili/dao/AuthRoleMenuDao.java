package com.lizejin.bilibili.dao;

import com.lizejin.bilibili.domain.auth.AuthRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public interface AuthRoleMenuDao {
    List<AuthRoleMenu> getAuthRoleMenuByRoleIds(@Param("roleIds") Set<Long> roleIds);
}

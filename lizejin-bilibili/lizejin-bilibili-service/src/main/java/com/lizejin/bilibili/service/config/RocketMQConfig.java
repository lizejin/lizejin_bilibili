package com.lizejin.bilibili.service.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lizejin.bilibili.domain.UserFollowing;
import com.lizejin.bilibili.domain.UserMoment;
import com.lizejin.bilibili.domain.constant.UserMomentsConstant;
import com.lizejin.bilibili.service.UserFollowingService;
import com.mysql.cj.util.StringUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.List;

//@Configuration
public class RocketMQConfig {

    @Value("${rocketmq.name.server.address}")
    private String nameServerAddr;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private UserFollowingService userFollowingService;
    //动态生产者
    @Bean("momentsProducer")
    public DefaultMQProducer momentsProducer() throws MQClientException {
        System.out.println("生产者启用");
        DefaultMQProducer producer = new DefaultMQProducer(UserMomentsConstant.GROUP_MOMENTS);
        producer.setNamesrvAddr(nameServerAddr);
        producer.start();
        return producer;
    }
    //动态消费者
    @Bean("momentConsumer")
    public DefaultMQPushConsumer momentConsumer() throws Exception{
        System.out.println("消费者启用");
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(UserMomentsConstant.GROUP_MOMENTS);
        consumer.setNamesrvAddr(nameServerAddr);
        consumer.subscribe(UserMomentsConstant.TOPIC_MOMENTS,"*");
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                MessageExt msg = msgs.get(0);
                if(msg==null){
                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                }
                String bodyStr = new String(msg.getBody()); //获取消息
                //将消息转换成动态对象
                UserMoment userMoment = JSONObject.toJavaObject(JSONObject.parseObject(bodyStr), UserMoment.class);
                //获取发送这条动态的up主id
                Long upId = userMoment.getUserId();
                //获取这个up主的粉丝
                List<UserFollowing> userFans = userFollowingService.getUserFans(upId);
                //遍历的结果就是每个用户关注的这个up主发的动态信息列表会保存至Redis中，之后通过String key = "subscribed-"+ fan.getUserId();
                //这个key可以获取到
                for(UserFollowing fan : userFans){
                    //这里给粉丝设置一个Redisid以便之后在Redis通过这个id来获取关注的动态数据
                    String key = "subscribed-"+ fan.getUserId();
                    //这里获取到的数据如果不为空，则是这个粉丝的之前获取到的动态列表，但这里转为字符串形式
                    String subscribedListStr = redisTemplate.opsForValue().get(key);
                    List<UserMoment> subscribedList;
                    //这里如果获取的数据为空，则可能这个用户刚关注up主没有受到之前的动态，所以需要设置一个新的动态列表
                    if(StringUtils.isNullOrEmpty(subscribedListStr)){
                        subscribedList = new ArrayList<>();
                        System.out.println("现在才来获取");
                    }else {
                        //这里是原先的动态列表，这里解析成集合形式
                        subscribedList = JSONArray.parseArray(subscribedListStr,UserMoment.class);
                        System.out.println("过去加现在");
                    }
                    //加入本次新的动态消息
                    subscribedList.add(userMoment);
                    //将当前用户关注的up主发的动态数据重新覆盖至redis
                    redisTemplate.opsForValue().set(key,JSONObject.toJSONString(subscribedList));
                    System.out.println(subscribedList.toString());
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
        return consumer;
    }

}

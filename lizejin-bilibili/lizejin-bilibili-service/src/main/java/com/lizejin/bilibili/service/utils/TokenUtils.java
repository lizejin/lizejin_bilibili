package com.lizejin.bilibili.service.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.lizejin.bilibili.domain.exception.ConditionException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;


public class TokenUtils {

    private static final String ISSUER = "李泽金";
    //根据用户id生成token令牌
    public static String generateToken(Long userId) throws Exception {
        Algorithm algorithm = Algorithm.RSA256(RSAUtil.getPublicKey(),RSAUtil.getPrivateKey());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_WEEK,7);
        return JWT.create().withKeyId(String.valueOf(userId))
                .withIssuer(ISSUER).withExpiresAt(calendar.getTime()).sign(algorithm);
    }
    //根据token令牌验证获取用户id
    public static Long verifyToken(String token){
        try {
            Algorithm algorithm = Algorithm.RSA256(RSAUtil.getPublicKey(),RSAUtil.getPrivateKey());
            JWTVerifier verifier = JWT.require(algorithm).build();
            //验证token
            DecodedJWT jwt = verifier.verify(token);
            String keyId = jwt.getKeyId();
            return Long.valueOf(keyId);
        }catch (TokenExpiredException e){
            throw new ConditionException("555","token过期了！");
        }catch (Exception e){
            throw new ConditionException("非法用户token！");
        }
    }
}

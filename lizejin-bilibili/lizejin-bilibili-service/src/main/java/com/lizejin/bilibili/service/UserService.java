package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.UserDao;
import com.lizejin.bilibili.domain.User;
import com.lizejin.bilibili.domain.UserFollowing;
import com.lizejin.bilibili.domain.UserInfo;
import com.lizejin.bilibili.domain.constant.UserConstant;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.utils.MD5Util;
import com.lizejin.bilibili.service.utils.RSAUtil;
import com.lizejin.bilibili.service.utils.TokenUtils;
import com.mysql.cj.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserFollowingService userFollowingService;

    public void addUser(Map<String,String> map){
        String nickName = map.get("nickName");
        String password = map.get("password");
        String phone = map.get("phone");
        //手机号码为空不能注册
        if(StringUtils.isNullOrEmpty(phone)){
             throw new ConditionException("手机号码不能为空！");
        }
        //该手机号是否注册过（通过用户手机号码查找是否有对应用户）
        User user = getUserByPhone(phone);
        if (user != null){
            throw new ConditionException("当前手机号码已注册！");
        }
        //设置相关属性值用来保存用户信息
        Date date = new Date();
        User newUser = new User();
        String salt = String.valueOf(date.getTime());
        //RSA解密，加以盐值进行MD5加密
        String rawPassword;
        try {
            rawPassword = RSAUtil.decrypt(password);
        } catch (Exception e) {
            throw new ConditionException("系统解密失败! ");
        }
        String md5Password = MD5Util.sign(rawPassword, salt, "UTF-8");
        newUser.setPassword(md5Password);
        newUser.setPhone(phone);
        newUser.setCreateTime(date);
        newUser.setSalt(salt);
        //保存用户表
        userDao.addNewUser(newUser);

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(newUser.getId());
        userInfo.setCreateTime(date);
        userInfo.setNick(nickName);
        userInfo.setBirth(UserConstant.DEFAULT_BIRTH);
        userInfo.setGender(UserConstant.GENDER_MALE);
        //保存用户信息
        userDao.addUserInfo(userInfo);
    }
   //通过用户手机号码查找是否有对应用户
    User getUserByPhone(String phone){
        User user = userDao.selectByPhone(phone);
        return user;
    }
    //登陆成功返回token令牌
    public String getToken(User user) throws Exception {
        String password = user.getPassword();
        String phone = user.getPhone();
        if(StringUtils.isNullOrEmpty(phone)){
            throw new ConditionException("手机号码不能为空！");
        }
        User userByPhone = this.getUserByPhone(phone);
        if(userByPhone == null){
            throw new ConditionException("当前手机号未注册过！");
        }
        String rawPassword;
        try {
            rawPassword = RSAUtil.decrypt(password);
        } catch (Exception e) {
            throw new ConditionException("解密失败！");
        }
        String sign = MD5Util.sign(rawPassword, userByPhone.getSalt(), "UTF-8");
        if(!userByPhone.getPassword().equals(sign)){
            throw new ConditionException("密码错误！");
        }
        return TokenUtils.generateToken(userByPhone.getId());
    }
    //获取用户以及用户详细信息（这里的user类里有个userrinfo类做属性）
    public User getUserInfoByUserId(Long currentUserId) {
        User userById = userDao.getUserById(currentUserId);
        UserInfo userInfoByUserId = userDao.getUserInfoByUserId(currentUserId);
        userById.setUserInfo(userInfoByUserId);
        return userById;
    }
    //更新用户信息
    public Integer updateUserInfos(UserInfo userInfo) {
        userInfo.setUpdateTime(new Date());
        return userDao.updateUserInfo(userInfo);
    }
    //获取用户
    public User getUserById(Long id){
        return userDao.getUserById(id);
    }
    //根据多个userid即userid数组查找对应的用户信息
    public List<UserInfo> getUserInfoByUserIds(Set<Long> followingId) {
        List<UserInfo> list = userDao.getUserInfoByUserIds(followingId);
        return list;
    }

    public Integer getCountByNick(String nick) {
        return userDao.getCountByNick(nick);
    }

    //分页查询的数据，注意数据返回的up主们，需要设置是否被当前用户关注过
    public List<UserInfo> getUserInfoByPageRecords(JSONObject params) throws JSONException {
        Map map = new HashMap();
        map.put("start",(params.getInt("no")-1)*params.getInt("size"));
        map.put("limit",params.getInt("size"));
        map.put("nick",params.getString("nick"));
        //分页查出模糊搜索的数据
        List<UserInfo> result = userDao.getUserInfoByPage(map);
        List<UserFollowing> userFollowingList = userFollowingService.getUserFollowings(params.getLong("userId"));
        for(UserInfo uInfo : result){
            uInfo.setFollowed(false);
            for (UserFollowing userFollowing: userFollowingList){
                if(uInfo.getUserId().equals(userFollowing.getFollowingId())){
                   uInfo.setFollowed(true);
                }
            }
        }
        return result;
    }

    public List<UserInfo> batchGetUserInfoByUserIds(Set<Long> userIdList) {
        return userDao.batchGetUserInfoByUserIds(userIdList);
    }
}

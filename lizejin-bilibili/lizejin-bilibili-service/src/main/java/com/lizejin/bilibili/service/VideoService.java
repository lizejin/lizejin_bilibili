package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.VideoDao;
import com.lizejin.bilibili.domain.*;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.utils.FastDFSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName VideoService
 * @Description 视频操作模块的service层
 * @Author lizejin
 * @Date 2023/4/16
 */
@Service
public class VideoService {

    @Autowired
    private VideoDao videoDao;
    @Autowired
    private FastDFSUtil fastDFSUtil;
    @Autowired
    private UserCoinService userCoinService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserFollowingService userFollowingService;

     /**
      * @Author @lizejin
      * @Description //视频投稿业务功能实现
      * @Date 13:29 2023/4/16
      * @param  video 投稿视频数据
      **/
    @Transactional
    public void addVideos(Video video) {
        video.setCreateTime(new Date());
        videoDao.addVideos(video);
        List<VideoTag> videoTagList = video.getVideoTagList();
        videoTagList.forEach(item->{
            item.setVideoId(video.getId());
            item.setCreateTime(new Date());
        });
        videoDao.batchAddVideoTags(videoTagList);
    }
   /**
    * @Author @lizejin
    * @Description //分页查询指定分区的视频分页数据业务
    * @Date 14:07 2023/4/16
    * @param  size,no,area 每页记录数，起始页，分区值
    **/
    public PageList<Video> getVideoListByPage(Integer size, Integer no, String area) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("limit",size);
        params.put("start",(no-1)*size);
        params.put("area",area);
        Integer total = videoDao.pageCountVideos(params);
        List<Video> videos = new ArrayList<>();
        if(total > 0){
            videos = videoDao.pageListVideos(params);
            videos.forEach(item->{
                List<VideoTag> videoTags = videoDao.getVideoTagsByVideoId(item.getId());
                item.setVideoTagList(videoTags);
            });
        }
        PageList<Video> page = new PageList<>();
        page.setTotal(total);
        page.setList(videos);

        return page;
    }

    public void viewVideoOnlineBySlices(HttpServletRequest request, HttpServletResponse response, String url) throws Exception {
        fastDFSUtil.viewVideoOnlineBySlices(request,response,url);
    }
    /**
     * @Author @lizejin
     * @Description //添加点赞记录
     * @Date 23:36 2023/4/16
     * @param  videoId,userId 视频id和用户id
     **/
    public void addVideoLikes(Long videoId, Long userId) {
        Video videoById = videoDao.getVideoById(videoId);
        if(videoById == null){
            throw new ConditionException("非法视频");
        }
        VideoLike videoLike = videoDao.getVideoLikeByVideoIdAndUserId(videoId, userId);
        if(videoLike != null){
            throw new ConditionException("您已赞过该视频");
        }
        VideoLike like = new VideoLike();
        like.setUserId(userId);
        like.setVideoId(videoId);
        like.setCreateTime(new Date());
        videoDao.addVideoLike(like);
    }
    /**
     * @Author @lizejin
     * @Description //取消点赞
     * @Date 23:45 2023/4/16
     **/
    
    public void deleteVideoLikes(Long videoId, Long userId) {
        videoDao.deleteVideoLike(videoId,userId);
    }
    /**
     * @Author @lizejin
     * @Description //查看指定视频id的点赞数
     * @Date 23:50 2023/4/16
     * @param  videoId 视频id
     **/
    public Map<String, Object> getVideoLikes(Long videoId,Long userId) {
        Long total = videoDao.getVideoLikes(videoId);
        VideoLike videoLike = videoDao.getVideoLikeByVideoIdAndUserId(videoId, userId);
        boolean result = false;
        if (videoLike!=null){
            result = true;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("count",total);
        map.put("like",result);
        return map;
    }
    /**
     * @Author @lizejin
     * @Description //添加视频收藏
     * @Date 10:14 2023/4/17
     * @param  videoCollection 收藏信息
     **/
    
    public void addVideoCollections(VideoCollection videoCollection) {
        Long videoId = videoCollection.getVideoId();
        Long groupId = videoCollection.getGroupId();
        if(videoId == null || groupId == null){
            throw new ConditionException("参数异常");
        }
        Video video = videoDao.getVideoById(videoId);
        if(video == null){
            throw new ConditionException("非法视频");
        }
        //不管收藏有没有，先删除后添加 【既实现了分组也实现了更新】
        videoDao.deleteVideoCollection(videoId,videoCollection.getUserId());
        videoCollection.setCreateTime(new Date());
        videoDao.addVideoCollection(videoCollection);
    }
    /**
     * @Author @lizejin
     * @Description //删除收藏记录
     * @Date 10:17 2023/4/17
     **/
    
    public void deleteVideoCollections(Long videoId, Long userId) {
        videoDao.deleteVideoCollection(videoId,userId);
    }
    /**
     * @Author @lizejin
     * @Description //获取收藏数
     * @Date 10:22 2023/4/17
     **/
    
    public Map<String, Object> getVideoCollectionsAmount(Long videoId, Long userId) {
        Long count = videoDao.getVideoCollections(videoId);
        VideoCollection videoCollection = videoDao.getVideoCollectionByVideoIdAndUserId(videoId, userId);
        boolean collection = false;
        if(videoCollection!=null){
            collection = true;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("count",count);
        map.put("like",collection);
        return map;
    }
    /**
     * @Author @lizejin
     * @Description //用户投币
     * @Date 11:17 2023/4/17
     * @param  videoCoin 投币信息
     **/
    
    @Transactional
    public void addVideoCoins(VideoCoin videoCoin) {
        Long videoId = videoCoin.getVideoId();
        Long userId = videoCoin.getUserId();
        //投币数
        Integer coinAmount = videoCoin.getAmount();
        if(videoId == null) {
            throw new ConditionException("参数异常");
        }
        Video video = videoDao.getVideoById(videoId);
        if(video == null){
            throw new ConditionException("非法视频");
        }
        Integer userCoinsAmount = userCoinService.getUserCoinsAmount(userId);
        if(userCoinsAmount < coinAmount){
            throw new ConditionException("您当前硬币数不足");
        }
        VideoCoin videoCoinByVideoIdAndUserId = videoDao.getVideoCoinByVideoIdAndUserId(videoId, userId);
        //根据用户投币表里有无投币记录进行新增和更改操作
        if(videoCoinByVideoIdAndUserId == null){
             videoCoin.setCreateTime(new Date());
             videoDao.addVideoCoin(videoCoin);
        }else {
            Integer amount = videoCoinByVideoIdAndUserId.getAmount();
            videoCoin.setAmount(amount + coinAmount);
            videoCoin.setUpdateTime(new Date());
            videoDao.updateVideoCoin(videoCoin);
        }
        //扣除用户账户里硬币数
        userCoinService.updateUserCoinsAmount(userId,userCoinsAmount-coinAmount);

    }
    /**
     * @Author @lizejin
     * @Description //查看视频投币数，区分用户模式和游客模式
     * @Date 11:20 2023/4/17
     **/
    
    public Map<String, Object> getVideoCoins(Long videoId, Long userId) {
        Long count = videoDao.getVideoCoinsAmount(videoId);
        VideoCoin videoCoinByVideoIdAndUserId = videoDao.getVideoCoinByVideoIdAndUserId(videoId, userId);
        boolean like = false;
        if(videoCoinByVideoIdAndUserId !=null){
            like = true;
        }
        Map<String ,Object> map = new HashMap<>();
        map.put("count",count);
        map.put("like",like);
        return map;
    }
    /**
     * @Author @lizejin
     * @Description //获取视频详情包括视频的基本信息和up主的基本信息
     * @Date 14:28 2023/4/17
     **/
    
    public Map<String, Object> getVideoDetails(Long videoId,Long userId) {
        Video videoDetails = videoDao.getVideoDetails(videoId);
        Long upId = videoDetails.getUserId();
        User userDetail = userService.getUserInfoByUserId(upId);
        UserInfo userInfo = userDetail.getUserInfo();
        //当前用户是否关注过这哥们
        if(userId != null){
            UserFollowing userFollowing = new UserFollowing();
            userFollowing.setUserId(userId);
            userFollowing.setFollowingId(upId);
            userInfo.setFollowed(userFollowingService.checkFollowing(userFollowing));
        }
        Map<String, Object> map = new HashMap<>();
        map.put("video",videoDetails);
        map.put("userInfo",userInfo);
        return map;
    }
    public void addVideoComment(VideoComment videoComment, Long userId) {
        Long videoId = videoComment.getVideoId();
        if(videoId == null){
            throw new ConditionException("参数异常！");
        }
        Video video = videoDao.getVideoById(videoId);
        if(video == null){
            throw new ConditionException("非法视频！");
        }
        videoComment.setUserId(userId);
        videoComment.setCreateTime(new Date());
        videoDao.addVideoComment(videoComment);
    }

   /**
    * @Author @lizejin
    * @Description //查看评论
    * @Date 21:50 2023/4/17
    **/
   
    public PageList<VideoComment> pageListVideoComments(Integer size, Integer no, Long videoId) {
        Video video = videoDao.getVideoById(videoId);
        if(video == null){
            throw new ConditionException("非法视频！");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("start", (no-1)*size);
        params.put("limit", size);
        params.put("videoId", videoId);
        Integer total = videoDao.pageCountVideoComments(params);
        List<VideoComment> list = new ArrayList<>();
        if(total > 0){
            list = videoDao.pageListVideoComments(params);
            //批量查询二级评论
            List<Long> parentIdList = list.stream().map(VideoComment::getId).collect(Collectors.toList());
            List<VideoComment> childCommentList = videoDao.batchGetVideoCommentsByRootIds(parentIdList);
            //批量查询用户信息
            Set<Long> userIdList = list.stream().map(VideoComment::getUserId).collect(Collectors.toSet());//父的userid
            Set<Long> replyUserIdList = childCommentList.stream().map(VideoComment::getUserId).collect(Collectors.toSet());//子评论里A回复B中A的id
            Set<Long> childUserIdList = childCommentList.stream().map(VideoComment::getReplyUserId).collect(Collectors.toSet());//子评论里A回复B中B的id;
            userIdList.addAll(replyUserIdList);
            userIdList.addAll(childUserIdList);
            List<UserInfo> userInfoList = userService.batchGetUserInfoByUserIds(userIdList);//所有的评论者的用户信息
            //根据id和信息对应【map操作牛啊，通过设置key 与value 之后通过key值拿数据很快，而不是使用遍历去操作】
            Map<Long, UserInfo> userInfoMap = userInfoList.stream().collect(Collectors.toMap(UserInfo :: getUserId, userInfo -> userInfo));
            list.forEach(comment -> {
                Long id = comment.getId(); //根节点id
                List<VideoComment> childList = new ArrayList<>();
                childCommentList.forEach(child -> {
                    if(id.equals(child.getRootId())){//如果是父子关系
                        child.setUserInfo(userInfoMap.get(child.getUserId())); //设置好回复者数据
                        child.setReplyUserInfo(userInfoMap.get(child.getReplyUserId()));//设置好被回复者数据
                        childList.add(child);
                    }
                });
                //设置二级评论的数据
                comment.setChildList(childList);
                //设置根评论的用户信息
                comment.setUserInfo(userInfoMap.get(comment.getUserId()));
            });
        }
        return new PageList<>(total, list);
    }

    /**
     * @Author @lizejin
     * @Description //评论逻辑仿照
     * @Date 15:03 2023/4/18
     **/
    public PageList<VideoComment> getVideoComments(Integer size,Integer no,Long videoId){
        if(videoId == null || size == null || no == null){
            throw new ConditionException("参数异常");
        }
        Video videoById = videoDao.getVideoById(videoId);
        if(videoById == null){
            throw new ConditionException("非法视频");
        }
        Map<String,Object> params = new HashMap<>();
        params.put("start",(no-1)*size);
        params.put("limit",size);
        params.put("videoId",videoId);
        Integer total = videoDao.pageCountVideoComments(params);
        List<VideoComment> rootComments = new ArrayList<>();
        if(total>0){
            rootComments = videoDao.pageListVideoComments(params);
            List<Long> rootIds = rootComments.stream().map(VideoComment::getId).collect(Collectors.toList());
            List<VideoComment> secondComments = videoDao.batchGetVideoCommentsByRootIds(rootIds);

            Set<Long> listId = rootComments.stream().map(VideoComment::getUserId).collect(Collectors.toSet());
            Set<Long> secondUserIds = secondComments.stream().map(VideoComment::getUserId).collect(Collectors.toSet());
            Set<Long> replyIds = secondComments.stream().map(VideoComment::getReplyUserId).collect(Collectors.toSet());
            listId.retainAll(secondUserIds);
            listId.retainAll(replyIds);
            List<UserInfo> userInfoList = userService.batchGetUserInfoByUserIds(listId);
            Map<Long, UserInfo> userInfoMap = userInfoList.stream().collect(Collectors.toMap(UserInfo::getUserId, userInfo -> userInfo));

            rootComments.forEach(item->{
                Long id = item.getId();
                List<VideoComment> childList = new ArrayList<>();
                secondComments.forEach(child->{
                    if(id.equals(child.getRootId())){
                        child.setUserInfo(userInfoMap.get(child.getUserId()));
                        child.setReplyUserInfo(userInfoMap.get(child.getReplyUserId()));
                        childList.add(child);
                    }
                });
                 item.setUserInfo(userInfoMap.get(item.getUserId()));
                 item.setChildList(childList);
            });
        }
        return new PageList<>(total,rootComments);
    }
    
}

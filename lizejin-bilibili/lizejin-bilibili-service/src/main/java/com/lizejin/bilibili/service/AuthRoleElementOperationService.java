package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.AuthRoleElementOperationDao;
import com.lizejin.bilibili.domain.auth.AuthRoleElementOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AuthRoleElementOperationService {

    @Autowired
    private AuthRoleElementOperationDao authRoleElementOperationDao;

    public List<AuthRoleElementOperation> getAuthRoleElementOperationsByRoleIds(Set<Long> roleIds) {
       return authRoleElementOperationDao.getAuthRoleElementOperationsByRoleIds(roleIds);
    }


}

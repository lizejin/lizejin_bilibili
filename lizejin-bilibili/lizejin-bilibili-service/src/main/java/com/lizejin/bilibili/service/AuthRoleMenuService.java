package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.AuthRoleMenuDao;
import com.lizejin.bilibili.domain.auth.AuthRoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AuthRoleMenuService {

    @Autowired
    private AuthRoleMenuDao authRoleMenuDao;
    
    public List<AuthRoleMenu> getAuthRoleMenuByRoleIds(Set<Long> roleIds) {
        return authRoleMenuDao.getAuthRoleMenuByRoleIds(roleIds);
    }
}

package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.UserRoleDao;
import com.lizejin.bilibili.domain.auth.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleService {
    @Autowired
    private UserRoleDao userRoleDao;

    public List<UserRole> getUserRoleByUserId(Long userId){
        List<UserRole> list = userRoleDao.getUserRoleByUserId(userId);
        return list;
    }
}

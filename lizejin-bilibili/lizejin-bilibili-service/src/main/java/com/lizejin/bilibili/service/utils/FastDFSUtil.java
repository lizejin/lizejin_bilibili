package com.lizejin.bilibili.service.utils;

import com.github.tobato.fastdfs.domain.fdfs.FileInfo;
import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.AppendFileStorageClient;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@Component
public class FastDFSUtil {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Autowired
    private AppendFileStorageClient appendFileStorageClient;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Value("${upload.slice.file.path}")
    private String uploadSliceFilePath;
    @Value("${fdfs.storage.path}")
    private String storagePath;
    private static final String DEFAULT_GROUP = "group1";
    private static final String PATH_KEY = "path-key:";
    private static final String UPLOAD_SIZE_KEY = "upload-size-key:";
    private static final String UPLOAD_NO_KEY = "upload-no-key:";
    private static final int SLICE_SIZE = 1024*1024*2; //2M
    //获取文件类型
    public String getFileType(MultipartFile file){
        if(file == null){
            throw new ConditionException("非法文件！");
        }
        String fullName = file.getOriginalFilename();
        int i = fullName.lastIndexOf(".");
        return fullName.substring(i+1);
    }
    //文件一般上传
    public String uploadCommonFile(MultipartFile file) throws IOException {
        InputStream inputStream = file.getInputStream();
        long size = file.getSize();
        String fileType = this.getFileType(file);
        Set<MetaData> metaDataSet = new HashSet<>();
        StorePath storePath = fastFileStorageClient.uploadFile(inputStream, size, fileType, metaDataSet);
        return storePath.getPath();//这里的path不包括组别
    }
    //支持断点续传的文件上传【一般用在第一个片段的上传】
   public String uploadAppenderFile(MultipartFile file) throws IOException {
       String fileType = this.getFileType(file);
       StorePath storePath = appendFileStorageClient.uploadAppenderFile(DEFAULT_GROUP, file.getInputStream(),
                             file.getSize(), fileType);
       return storePath.getPath();
   }
    //支持断点续传的文件的追加/修改
    public void modifyAppenderFile(MultipartFile file,String path,long offset) throws IOException {
        appendFileStorageClient.modifyFile(DEFAULT_GROUP,path,file.getInputStream(),file.getSize(),offset);
    }
    //文件删除
    public void deleteFile(String path){
        fastFileStorageClient.deleteFile(path);
    }

    //上传分片文件的操作
    //这里的file是分片文件，之后前端每次上传分片是就调用接口去调用这个方法，就可以实现断点续传
    public String uploadFileBySlices(MultipartFile file,String fileMD5,Integer sliceNo,Integer total) throws IOException {
        if(file == null || total == null || sliceNo == null){
            throw new ConditionException("参数异常");
        }
        //在redis存资源上传路径的key，文件上传时的状态大小，文件是第几片的key值
        String pathKey = PATH_KEY + fileMD5;
        String uploadSizeKey = UPLOAD_SIZE_KEY + fileMD5;
        String uploadNoKey = UPLOAD_NO_KEY+fileMD5;
        String uploadSize = redisTemplate.opsForValue().get(uploadSizeKey);
        Long uploadSizeNow = 0L;
        //这里目的是当上传第二片时或者其他片段时，需要获取当前上传状态已经上传了多少的文件
        if(!StringUtils.isNullOrEmpty(uploadSize)){
             uploadSizeNow = Long.valueOf(uploadSize);
        }
        String fileType = this.getFileType(file);
        //上传的是第一片文件就调用.uploadAppenderFile(file);并在redis服务器做相关设置
        if(sliceNo == 1){
            String storagePath = this.uploadAppenderFile(file);
            if(StringUtils.isNullOrEmpty(storagePath)){
                throw new ConditionException("上传失败");
            }
            redisTemplate.opsForValue().set(pathKey,storagePath);
            redisTemplate.opsForValue().set(uploadNoKey,"1");
        }else {//当上传的文件片段为其他片时，调用modifyAppenderFile方法，将文件追加到在这个storagePath路径下偏移量
            // 为uploadSizeNow这么个尺寸大小的位置
            String storagePath = redisTemplate.opsForValue().get(pathKey);
            if (StringUtils.isNullOrEmpty(storagePath)){
                throw new ConditionException("上传失败");
            }
            redisTemplate.opsForValue().set(uploadNoKey,String.valueOf(sliceNo));
            this.modifyAppenderFile(file,storagePath,uploadSizeNow);
        }
        uploadSizeNow += file.getSize();
        redisTemplate.opsForValue().set(uploadSizeKey,String.valueOf(uploadSizeNow));

        /*String sliceNoNow = redisTemplate.opsForValue().get(uploadNoKey);
        Integer sliceNow = Integer.valueOf(sliceNoNow);*/
        String resultPath = "";
        //当上传的总片数等于总片数则证明上传完毕，此时将redis存的相关key值删除，因为不需要用了
        if(sliceNo.equals(total)){
            resultPath = redisTemplate.opsForValue().get(pathKey);
            List<String> list = Arrays.asList(pathKey, uploadSizeKey, uploadNoKey);
            redisTemplate.delete(list);
        }
        return resultPath; //当文件没上传完毕，是不会返回文件保存路径的
    }
    //文件分片
    public void ConvertFileToSlice(MultipartFile multipartFile) throws IOException {
        String fileType = this.getFileType(multipartFile);
        File file = this.mutipartFileConvertToFile(multipartFile);
        int count = 1;
        //将文件分片保存至上传路径，以SLICE_SIZE为分割尺寸
        for(int i=0;i<file.length();i+=SLICE_SIZE){
            //这里的RandomAccessFile随机访问文件类提供了文件的读写操作，这个类的好处就是可以指定位置去读写
            RandomAccessFile accessFile = new RandomAccessFile(file,"r");
            accessFile.seek(i); //从i位置开始操作
            byte[] bytes = new byte[SLICE_SIZE];
            int len = accessFile.read(bytes); //读取这部分的文件
            String path = uploadSliceFilePath+count+"."+fileType;
            File slice = new File(path);
            //保存至路径
            FileOutputStream fos = new FileOutputStream(slice);
            fos.write(bytes,0,len); //得到片段文件
            fos.close();
            accessFile.close();
            count++;
        }
        //将转换得到的临时文件删除
        file.delete();
    }


    //MutipartFile文件转File
    public File mutipartFileConvertToFile(MultipartFile file) throws IOException {
        String fullName = file.getOriginalFilename();
        String[] fileName = fullName.split("\\.");
        File tempFile = File.createTempFile(fileName[0], "." + fileName[1]);
        file.transferTo(tempFile);//在磁盘生成临时文件
        return tempFile;
    }
    
    /**
     * @Author @lizejin
     * @Description //视频在线播放的工具类
     * @Date 20:45 2023/4/16
     * @param  request,response,path 请求，响应，文件服务器文件资源路径
     **/
    public void viewVideoOnlineBySlices(HttpServletRequest request,
                                        HttpServletResponse response,
                                        String path) throws Exception{
        FileInfo fileInfo = fastFileStorageClient.queryFileInfo(DEFAULT_GROUP, path);
        long fileSize = fileInfo.getFileSize();
        String url = storagePath+path;
        //获取请求头
        Enumeration<String> headerNames = request.getHeaderNames();
        Map<String ,Object> headers = new HashMap<>();
        while (headerNames.hasMoreElements()){
            String headerName = headerNames.nextElement();
            headers.put(headerName,request.getHeader(headerName));
        }
        //根据前端对视频片段大小的需求，设置不同的begin和end
        String rangeStr = request.getHeader("Range");
        long begin = 0L;
        if(StringUtils.isNullOrEmpty(rangeStr)){
            rangeStr = "bytes=0-"+(fileSize-1);
        }
        String[] range = rangeStr.split("bytes=|-");
        if(range.length >= 2){
            begin = Long.parseLong(range[1]);
        }
        long end = fileSize-1;
        if(range.length >= 3){
            end = Long.parseLong(range[2]);
        }
        long len = end-begin;
        //设置响应头的数据
        String contentRange = "bytes " + begin + "-" + end + "/" + fileSize;
        response.setHeader("Content-Range", contentRange);
        response.setHeader("Accept-Ranges", "bytes");
        response.setHeader("Content-Type", "video/mp4");
        response.setContentLength((int)len);
        response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        //发送请求
        HttpUtil.get(url,headers,response);
    }
    
}

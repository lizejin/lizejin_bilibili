package com.lizejin.bilibili.service.handler;

import com.lizejin.bilibili.domain.JsonResponse;
import com.lizejin.bilibili.domain.exception.ConditionException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CommonGlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public JsonResponse<String> commonGlobalExceptionHandler(HttpServletRequest request,Exception ex){
        String errorMsg = ex.getMessage();
        //当手动抛出ConditionException时做这一步
        if(ex instanceof ConditionException){
            String code = ((ConditionException) ex).getCode();
            return new JsonResponse<>(code,errorMsg);
        }else {//一般情况做下面这个
            return new JsonResponse<>("500",errorMsg);
        }
    }

}

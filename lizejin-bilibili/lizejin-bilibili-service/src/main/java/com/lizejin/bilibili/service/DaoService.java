package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.DemoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DaoService {

    @Autowired
    private DemoDao demoDao;

    public String getName(int id){
        String name = demoDao.getName(id);
        return name;
    }
}

package com.lizejin.bilibili.service;

import com.lizejin.bilibili.domain.auth.AuthRoleElementOperation;
import com.lizejin.bilibili.domain.auth.AuthRoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AuthRoleService {
    @Autowired
    private AuthRoleElementOperationService authRoleElementOperationService;
    @Autowired
    private AuthRoleMenuService authRoleMenuService;

    public List<AuthRoleElementOperation> getAuthRoleElementOperationsByRoleIds(Set<Long> roleIds){
       return authRoleElementOperationService.getAuthRoleElementOperationsByRoleIds(roleIds);
    }

    public List<AuthRoleMenu> getAuthRoleMenuByRoleIds(Set<Long> roleIds){
       return authRoleMenuService.getAuthRoleMenuByRoleIds(roleIds);
    }
}

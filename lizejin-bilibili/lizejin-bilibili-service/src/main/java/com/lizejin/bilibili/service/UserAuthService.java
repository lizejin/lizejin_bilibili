package com.lizejin.bilibili.service;

import com.lizejin.bilibili.domain.auth.AuthRoleElementOperation;
import com.lizejin.bilibili.domain.auth.AuthRoleMenu;
import com.lizejin.bilibili.domain.auth.UserAuthorities;
import com.lizejin.bilibili.domain.auth.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserAuthService {

    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private AuthRoleService authRoleService;
    
    public UserAuthorities getUserAuthorities(Long userId) {
        //查找用户的角色，根据角色id查页面元素和菜单
        List<UserRole> userRoleList = userRoleService.getUserRoleByUserId(userId);
        Set<Long> roleIds = userRoleList.stream().map(UserRole::getRoleId).collect(Collectors.toSet());
        //查出操作元素
        List<AuthRoleElementOperation> authRoleElementOperations = authRoleService.getAuthRoleElementOperationsByRoleIds(roleIds);
        //查出菜单
        List<AuthRoleMenu> authRoleMenu = authRoleService.getAuthRoleMenuByRoleIds(roleIds);
        UserAuthorities userAuthorities = new UserAuthorities();
        userAuthorities.setRoleElementOperationList(authRoleElementOperations);
        userAuthorities.setRoleMenuList(authRoleMenu);
        //权限对象返回
        return userAuthorities;
    }
}

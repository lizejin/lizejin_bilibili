package com.lizejin.bilibili.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lizejin.bilibili.dao.UserMomentDao;
import com.lizejin.bilibili.domain.UserMoment;
import com.lizejin.bilibili.domain.constant.UserMomentsConstant;
import com.lizejin.bilibili.service.utils.RocketMQUtils;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class UserMomentService {

    @Autowired
    private UserMomentDao userMomentDao;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

 /*  //添加动态，并使动态生产者生产消息到对应主题
    public void addUserMoment(UserMoment userMoment) throws Exception {
        userMomentDao.addUserMoment(userMoment);
        DefaultMQProducer producer = (DefaultMQProducer) applicationContext.getBean("momentsProducer");
        Message message = new Message(UserMomentsConstant.TOPIC_MOMENTS, JSONObject.toJSONString(userMoment).getBytes(StandardCharsets.UTF_8));
        RocketMQUtils.syncSendMsg(producer,message);
    }
    //在redis上查询这个用户订阅的动态消息
    public List<UserMoment> getUserMomentsList(Long userId) {
        String key = "subscribed-"+userId;
        String listStr = redisTemplate.opsForValue().get(key);
        return JSONArray.parseArray(listStr,UserMoment.class);//将数据解析成List<UserMoment>
    }*/
}

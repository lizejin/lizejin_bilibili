package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.FollowingGroupDao;
import com.lizejin.bilibili.dao.UserFollowingDao;
import com.lizejin.bilibili.domain.FollowingGroup;
import com.lizejin.bilibili.domain.User;
import com.lizejin.bilibili.domain.UserFollowing;
import com.lizejin.bilibili.domain.UserInfo;
import com.lizejin.bilibili.domain.constant.UserConstant;
import com.lizejin.bilibili.domain.exception.ConditionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserFollowingService {

    @Autowired
    private FollowingGroupService followingGroupService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserFollowingDao userFollowingDao;

    //关注up主或关注后选择分组
    @Transactional
    public void addUserFollowing(UserFollowing userFollowing) {
        Long groupId = userFollowing.getGroupId();
        //分组id在不在，不在就默认设默认分组的id为groupid,在就查查分组在不在，不在抛出异常
        if (groupId == null) {
            FollowingGroup followingGroup = followingGroupService.getByType(UserConstant.USER_FOLLOWING_GROUP_TYPE_DEFAULT);
            userFollowing.setGroupId(followingGroup.getId());
        } else {
            FollowingGroup followingGroup = followingGroupService.getById(groupId);
            if (followingGroup == null) {
                throw new ConditionException("所选分组不存在！");
            }
        }
        //判断up主是否还存在
        Long followingId = userFollowing.getFollowingId();
        User user = userService.getUserById(followingId);
        if (user == null) {
            throw new ConditionException("你关注的up主不存在");
        }
        //以下是关注操作
        //考虑到更新和新增操作，这里先删除再新增，也可以根据分组id是否为空来操作，非空则更新，空则新增
        userFollowingDao.deleteByUserIdAndFollowingId(userFollowing.getUserId(), followingId);
        userFollowing.setCreateTime(new Date());
        userFollowingDao.addUserFollowing(userFollowing);
    }

    //获取用户关注列表
    //用户id查出该用户关注的所有up主
    //查出每个up主的信息
    //将关注的列表按分组组分类
    public List<FollowingGroup> getUserFollowing(Long userId){
        //根据用户id查出这个用户关注了那些up主
        List<UserFollowing> userFollowingList= userFollowingDao.getUserFollowingByUserId(userId);
        //拿到这些关注的up主的id，以集合形式存放
        Set<Long> followingId = userFollowingList.stream().map(UserFollowing::getFollowingId).collect(Collectors.toSet());
        List<UserInfo> userInfos = new ArrayList<>();
        //如果该用户有关注过up主，现在就根据这些id查出这些up主的信息
        if (userFollowingList.size()>0){
            userInfos = userService.getUserInfoByUserIds(followingId);
        }
        //UserFollowing添加一个用户信息属性，遍历数组当有用户id与up主的id一致时，将这条用户信息存到这个UserFollowing类中
        for(UserFollowing userFollow : userFollowingList){
            for(UserInfo userInfo : userInfos){
                if(userFollow.getFollowingId().equals(userInfo.getUserId())){
                    userFollow.setUserInfo(userInfo);
                }
            }
        } //此时的关注的up主followinggroup中就带有对应followingid的用户信息了

        //查出对应用户id下的所有分组，这里设计了系统默认设好的三个分组（没有与用户id绑定，自定分组就有）
        List<FollowingGroup> groupList = followingGroupService.getGroupsByUserId(userId);
        //前端要的是分组名和分组里的up主数据

        //这里设置全部关注的分组
        FollowingGroup allFollow = new FollowingGroup();
        allFollow.setName(UserConstant.USER_FOLLOWING_GROUP_ALL_NAME);
        allFollow.setFollowingUserInfoList(userInfos);//分组名，分组下的用户数据，这里是全部关注分组
        List<FollowingGroup> result = new ArrayList<>();
        result.add(allFollow);
        //以下根据分组放入对应的up信息
        for(FollowingGroup group : groupList){
           List<UserInfo> Info = new ArrayList<>();
           for(UserFollowing following : userFollowingList){
               //如果这个up主属于当前分组就把用户信息交个这个分组
               if (group.getId().equals(following.getGroupId())){
                   Info.add(following.getUserInfo());
               }
           }
           //分组里设置up主们的信息
           group.setFollowingUserInfoList(Info);
           //将分组信息存到返回结果中去
            result.add(group);
        }

        return result;
    }

    //获取粉丝列表
    //在userfollowing表中userID和followingID可以看成A关注了B，所以A是B的粉丝
    //则当要查好多粉丝时，以followingID为条件就可以查出对应的粉丝，现在提供的userID即当前用户的id，先把他当成followingID就能查出他的粉丝列表了
    //只是参数名字看起来有点怪
    //现在提供的userID作为条件，
    //查出粉丝列表，查出粉丝信息，粉丝信息与粉丝列表一一对应，并设置好是否互粉的标志
    public List<UserFollowing> getUserFans(Long userId){
        List<UserFollowing> fansList = userFollowingDao.getUserFansByUserId(userId);
        Set<Long> fanIds = fansList.stream().map(UserFollowing::getUserId).collect(Collectors.toSet());
        List<UserInfo> fanInfo = new ArrayList<>();
        if(fanIds.size()>0){
            fanInfo = userService.getUserInfoByUserIds(fanIds);
        }
        List<UserFollowing> userFollowingList = userFollowingDao.getUserFollowingByUserId(userId);

        for(UserFollowing fan : fansList){
            for(UserInfo userInfo : fanInfo){
                if(fan.getUserId().equals(userInfo.getUserId())){
                    userInfo.setFollowed(false);
                    fan.setUserInfo(userInfo);
                }
            } //完成到这步即一次循环，就有一个粉丝和粉丝信息结合成功，而这里的粉丝还没进行是否互关判断

            //这一步设置当粉丝列表中的userID和关注列表的followingID一致时，可判断出双方是互关的状态
            for(UserFollowing following : userFollowingList){
                if(following.getFollowingId().equals(fan.getUserId())){
                    fan.getUserInfo().setFollowed(true);
                }
            }
        }
        
        return fansList;
    }

    //添加分组
    public void addUserFollowingGroup(FollowingGroup group) {
        boolean checkGroup = this.getUserFollowingGroupByName(group.getName());
        if(checkGroup){
            throw new ConditionException("当前分组已存在，请更换分组名！");
        }
        group.setCreateTime(new Date());
        group.setType(UserConstant.USER_FOLLOWING_GROUP_TYPE_USER);
        userFollowingDao.addUserFollowingGroup(group);
    }
    //根据分组名查找当前分组是否存在
    public boolean getUserFollowingGroupByName(String name) {
        int result = userFollowingDao.getUserFollowingGroupByName(name);
        if(result>0){
            return true;
        }
        return false;
    }
    //根据当前用户id查出分组表的数据
    public List<FollowingGroup> getUserFollowingGroupByUserId(Long userId) {
        return userFollowingDao.getUserFollowingGroupByUserId(userId);
    }
    //根据用户id查出当前用户关注的up主
    public List<UserFollowing> getUserFollowings(Long userId){
        return userFollowingDao.getUserFollowingByUserId(userId);
    }
    //查找当前用户有没有关注这个up主
    public boolean checkFollowing(UserFollowing userFollowing) {
        UserFollowing userFollow = userFollowingDao.checkFollowing(userFollowing);
        if(userFollow==null){
            return false;
        }
        return true;
    }
    //取消关注
    public void cancleFollowing(UserFollowing userFollowing) {
        userFollowingDao.cancleFollowing(userFollowing);
    }
}

package com.lizejin.bilibili.service;

import com.lizejin.bilibili.dao.FollowingGroupDao;
import com.lizejin.bilibili.domain.FollowingGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowingGroupService {

    @Autowired
    private FollowingGroupDao followingGroupDao;

    //根据id查分组
    public FollowingGroup getById(Long id){
        return followingGroupDao.getById(id);
    }
    //根据type查分组
    public FollowingGroup getByType(String type){
        return followingGroupDao.getByType(type);
    }
    //根据用户id查找关注的up主
    public List<FollowingGroup> getGroupsByUserId(Long userId) {
        return followingGroupDao.getGroupsByUserId(userId);
    }
}



package com.lizejin.bilibili.api;

import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.*;
import com.lizejin.bilibili.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @ClassName VideoApi
 * @Description 视频模块接口controller层
 * @Author lizejin
 * @Date 2023/4/16
 */
@RestController
public class VideoApi {

    @Autowired
    private VideoService videoService;
    @Autowired
    private UserSupport userSupport;
    /**
     * @Author @lizejin
     * @Description //视频投稿
     * @Date 13:51 2023/4/16
     **/
    @PostMapping("/videos")
    public JsonResponse<String> addVideos(@RequestBody Video video){
        Long userId = userSupport.getCurrentUserId();
        video.setUserId(userId);
        videoService.addVideos(video);
        return JsonResponse.success();
    }
    /**
     * @Author @lizejin
     * @Description //分页查询指定分区的视频分页数据
     * @Date 14:03 2023/4/16
     * @param  size,no,area 每页记录数，起始页，分区值
     **/
    
    @GetMapping("/videos")
    public JsonResponse<PageList<Video>> getVideoListByPage(Integer size, Integer no, String area){
        PageList<Video> pageList = videoService.getVideoListByPage(size,no,area);
        return new JsonResponse<>(pageList);
    }
   /**
    * @Author @lizejin
    * @Description //视频在线播放功能的实现
    * @Date 23:31 2023/4/16
    * @param  request,url,response 请求的请求头中带有range即请求视频片段的范围
    **/
   
    @GetMapping("/video-slices")
    public void viewVideoOnlineBySlices(HttpServletRequest request,
                                        HttpServletResponse response,
                                        String url) throws Exception {
        videoService.viewVideoOnlineBySlices(request, response, url);
    }
    /**
     * @Author @lizejin
     * @Description //视频点赞
     * @Date 23:42 2023/4/16
     * @param  videoId 视频id
     **/
    
    @PostMapping("/video-likes")
    public JsonResponse<String> addVideoLikes(@RequestParam Long videoId){
        Long userId = userSupport.getCurrentUserId();
        videoService.addVideoLikes(videoId,userId);
        return JsonResponse.success();
    }
     /**
      * @Author @lizejin
      * @Description //取消点赞
      * @Date 23:46 2023/4/16
      * @param  
      **/
    @DeleteMapping("/video-likes")
    public JsonResponse<String> deleteVideoLikes(@RequestParam Long videoId){
        Long userId = userSupport.getCurrentUserId();
        videoService.deleteVideoLikes(videoId,userId);
        return JsonResponse.success();
    }
   /**
    * @Author @lizejin
    * @Description //获取点赞数
    * @Date 23:47 2023/4/16
    **/
   
    @GetMapping("/video-likes")
    public JsonResponse<Map<String,Object>> getVideoLikes(@RequestParam Long videoId){
        Long userId = null;
        try {
            userId = userSupport.getCurrentUserId();
        }catch (Exception ignored){}
        Map<String,Object> result = videoService.getVideoLikes(videoId,userId);
        return new JsonResponse<>(result);
    }
   /**
    * @Author @lizejin
    * @Description //视频收藏
    * @Date 10:14 2023/4/17
    **/
   
    @PostMapping("/video-collections")
    public JsonResponse<String> addVideoCollections(@RequestBody VideoCollection videoCollection){
        Long userId = userSupport.getCurrentUserId();
        videoCollection.setUserId(userId);
        videoService.addVideoCollections(videoCollection);
        return JsonResponse.success();
    }
    /**
     * @Author @lizejin
     * @Description //取消收藏
     * @Date 10:18 2023/4/17
     **/
    
    @DeleteMapping("/video-collections")
    public JsonResponse<String> deleteVideoCollections(@RequestParam Long videoId){
        Long userId = userSupport.getCurrentUserId();
        videoService.deleteVideoCollections(videoId,userId);
        return JsonResponse.success();
    }
    /**
     * @Author @lizejin
     * @Description //查看视频收藏数，区分用户模式和游客模式
     * @Date 10:28 2023/4/17
     * @param  
     **/
    @GetMapping("/video-collections")
    public JsonResponse<Map<String,Object>> getVideoCollectionsAmount(@RequestParam Long videoId){
        Long userId = null;
        try {
           userId = userSupport.getCurrentUserId();
        }catch (Exception ignored){}
        Map<String,Object> result = videoService.getVideoCollectionsAmount(videoId,userId);
        return new JsonResponse<>(result);
    }
    /**
     * @Author @lizejin
     * @Description //用户投币
     * @Date 11:17 2023/4/17
     **/
    
    @PostMapping("/video-coins")
    public JsonResponse<String> addVideoCoins(@RequestBody VideoCoin videoCoin){
        Long userId = userSupport.getCurrentUserId();
        videoCoin.setUserId(userId);
        videoService.addVideoCoins(videoCoin);
        return JsonResponse.success();
    }
    /**
     * @Author @lizejin
     * @Description //查看硬币数
     * @Date 11:25 2023/4/17
     **/
    @GetMapping("/video-coins")
    public JsonResponse<Map<String ,Object>> getVideoCoins(@RequestParam Long videoId){
        Long userId = null;
        try{
            userId = userSupport.getCurrentUserId();
        }catch (Exception ignored){}
        Map<String ,Object> map = videoService.getVideoCoins(videoId,userId);
        return new JsonResponse<>(map);
    }
   /**
    * @Author @lizejin
    * @Description //获取视频详情包括视频的基本信息和up主的基本信息
    * @Date 14:29 2023/4/17
    **/
    @GetMapping("/video-details")
    public JsonResponse<Map<String ,Object>> getVideoDetails(@RequestParam Long videoId){
        Long userId = null;
        try{
            userId = userSupport.getCurrentUserId();
        }catch (Exception ignored){}
        Map<String ,Object> map = videoService.getVideoDetails(videoId,userId);
        return new JsonResponse<>(map);
    }
    /**
     * @Author @lizejin
     * @Description //添加评论
     * @Date 22:07 2023/4/17
     **/
    @PostMapping("/video-comments")
    public JsonResponse<String> addComment(@RequestBody VideoComment videoComment){
        Long userId = userSupport.getCurrentUserId();
        videoService.addVideoComment(videoComment,userId);
        return JsonResponse.success();
    }
    /**
     * @Author @lizejin
     * @Description //分页查看评论
     * @Date 22:08 2023/4/17
     **/
    @GetMapping("/video-comments")
    public JsonResponse<PageList<VideoComment>> pageListVideoComments(@RequestParam Integer size,
                                                              @RequestParam Integer no,
                                                              @RequestParam Long videoId){
        PageList<VideoComment> videoCommentPageList = videoService.pageListVideoComments(size, no, videoId);
        return new JsonResponse<>(videoCommentPageList);
    }


}

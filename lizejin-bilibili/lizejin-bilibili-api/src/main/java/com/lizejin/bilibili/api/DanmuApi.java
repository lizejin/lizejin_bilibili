package com.lizejin.bilibili.api;

import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.Danmu;
import com.lizejin.bilibili.domain.JsonResponse;
import com.lizejin.bilibili.service.DanmuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName DanmuApi
 * @Description TODO
 * @Author lizejin
 * @Date 2023/4/20
 */
@RestController
public class DanmuApi {

    @Autowired
    private DanmuService danmuService;
    @Autowired
    private UserSupport userSupport;

    @GetMapping("/danmus")
    public JsonResponse<List<Danmu>> getdanmus(@RequestParam Long videoId,
                                               String startTime,String endTime) throws Exception {
        //游客模式下无法根据时间段查弹幕，而用户模式下可以
        List<Danmu> list = null;
        try {
            Long userId = userSupport.getCurrentUserId();
            list = danmuService.getDanmus(videoId, startTime, endTime);
            System.out.println("查找弹幕开始啦");
        }catch (Exception ignored){
            list = danmuService.getDanmus(videoId, null, null);
        }
        return new JsonResponse<>(list);
    }
}

package com.lizejin.bilibili.api.aspect;

import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.annotation.APILimitedRole;
import com.lizejin.bilibili.domain.auth.UserRole;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.UserRoleService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Order(1)
@Component
@Aspect
public class APILimitedRoleAspect {

    @Autowired
    private UserSupport userSupport;
    @Autowired
    private UserRoleService userRoleService;
    //切入点，这里的切入点为使用这个APILimitedRole注解的连接点
    @Pointcut("@annotation(com.lizejin.bilibili.domain.annotation.APILimitedRole)")
    public void check(){
    }
    //给切点编写切面程序
    @Before("check() && @annotation(apiLimitedRole)")
    public void doBefore(JoinPoint joinPoint,APILimitedRole apiLimitedRole ){
        Long userId = userSupport.getCurrentUserId();
        List<UserRole> userRole = userRoleService.getUserRoleByUserId(userId);
        String[] limitedRoleCode = apiLimitedRole.limitedRoleCodeLists();
        Set<String> limitedCode = Arrays.stream(limitedRoleCode).collect(Collectors.toSet());
        Set<String> collect = userRole.stream().map(UserRole::getRoleCode).collect(Collectors.toSet());
        //将collect与limitedRoleCode限制角色进行交集运算结果赋为collect，如果collect存在值，即当前用户是被等级限制的用户
        collect.retainAll(limitedCode);
        if (collect.size()>0){
            throw new ConditionException("您当前等级低，无权限操作此功能");
        }

    }


}

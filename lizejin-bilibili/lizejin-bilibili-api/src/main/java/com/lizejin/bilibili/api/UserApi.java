package com.lizejin.bilibili.api;

import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.*;
import com.lizejin.bilibili.domain.PageList;
import com.lizejin.bilibili.domain.annotation.APILimitedRole;
import com.lizejin.bilibili.domain.annotation.DataLimited;
import com.lizejin.bilibili.domain.constant.AuthRoleConstant;
import com.lizejin.bilibili.domain.constant.UserConstant;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.FollowingGroupService;
import com.lizejin.bilibili.service.UserFollowingService;
import com.lizejin.bilibili.service.UserService;
import com.lizejin.bilibili.service.utils.RSAUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserApi {

    @Autowired
    private UserService userService;
    @Autowired
    private UserSupport userSupport;
    @Autowired
    private UserFollowingService userFollowingService;
    @Autowired
    private FollowingGroupService followingGroupService;
    //返回公钥给前端进行加密处理
    @GetMapping("/rsa-pks")
    public JsonResponse<String> getRSAPublicKey(){
        String publicKeyStr = RSAUtil.getPublicKeyStr();
//        System.out.println("请求已处理"+publicKeyStr);
        return new JsonResponse<>(publicKeyStr);
    }

    //用户注册
    @PostMapping("/users")
    public JsonResponse<String> addUser(@RequestBody Map<String,String> map){
        userService.addUser(map);
        return JsonResponse.success();
    }
    //用户登录之获取token令牌
    @PostMapping("/user-tokens")
    public JsonResponse<String> loginToken(@RequestBody User user) throws Exception {
        String token = userService.getToken(user);
        return new JsonResponse<>(token);
    }
    //用户登录之根据token展示用户数据
    @GetMapping("/users")
    public JsonResponse<User> getUserDetail(){
        Long currentUserId = userSupport.getCurrentUserId();
        User user = userService.getUserInfoByUserId(currentUserId);
        return new JsonResponse<>(user);
    }
    //用户数据更新
    @PutMapping("/user-infos")
    public JsonResponse<String> updateUserInfo(@RequestBody UserInfo userInfo){
        Integer integer = userService.updateUserInfos(userInfo);
        if (integer<0){
            throw new ConditionException("更新失败，请刷新重试！");
        }
        return JsonResponse.success();
    }
    //关注up主或者取消关注
    @PostMapping("/user-followings")
    public JsonResponse<String> addUserFollow(@RequestBody UserFollowing userFollowing){
        Long currentUserId = userSupport.getCurrentUserId();
        userFollowing.setUserId(currentUserId);

        //查找改up主是否关注
        boolean check = userFollowingService.checkFollowing(userFollowing);
        if(check && userFollowing.getGroupId()==null){
            userFollowingService.cancleFollowing(userFollowing);
            return JsonResponse.success("取消关注成功！");
        }
        //关注操作或者分组---------------------------
        userFollowingService.addUserFollowing(userFollowing);
        return JsonResponse.success();
    }

    //获取分组信息
    @GetMapping("/user-followings")
    public JsonResponse<List<FollowingGroup>> getUserFollowings(){
        Long currentUserId = userSupport.getCurrentUserId();
        List<FollowingGroup> userFollowing = userFollowingService.getUserFollowing(currentUserId);
        return new JsonResponse<>(userFollowing);
    }//数据没有实现分页

    //获取粉丝列表
    @GetMapping("/user-fans")
    public JsonResponse<List<UserFollowing>> getUserFans(){
        Long currentUserId = userSupport.getCurrentUserId();
        List<UserFollowing> userFans = userFollowingService.getUserFans(currentUserId);
        return new JsonResponse<>(userFans);
    }

    //新增分组
    //限制lv0 的用户进行分组操作
    //限制新增分组的字段不能为“精彩关注”--简单的判断一下其实可以了，但是这里学习使用，即对于数据的值的控制
//    @APILimitedRole(limitedRoleCodeLists = {AuthRoleConstant.ROLE_LV0})
    @DataLimited
    @PostMapping("/user-following-groups")
    public JsonResponse<String> addGroup(@RequestBody FollowingGroup group){
        group.setUserId(userSupport.getCurrentUserId());
        userFollowingService.addUserFollowingGroup(group);
        return JsonResponse.success();
    }

    //获取自定义分组
    @GetMapping("/user-following-groups")
    public JsonResponse<List<FollowingGroup>> getUserFollowingGroups(){
        Long userId = userSupport.getCurrentUserId();
        List<FollowingGroup> groupList = userFollowingService.getUserFollowingGroupByUserId(userId);
        System.out.println("我不认为这个接口是必要的");
        return new JsonResponse<>(groupList);
    }

    //分页查询
    @GetMapping("/user-infos")
    public JsonResponse<PageList<UserInfo>> pageListUserInfo(Integer no,  String size, String nick) throws JSONException {
        Long userId = userSupport.getCurrentUserId();
        //本质是一种map，但是这里提供了比map更好的方法，例如不用手动强转类型
        JSONObject params = new JSONObject();
        params.put("no",no);
        params.put("size",size);
        params.put("nick",nick);
        params.put("userId",userId);
        //分页对象
        PageList<UserInfo> page = new PageList<>();
        //查找这个关键词的总记录数
        //分页查找这个关键词的up主
        //设置哪些是当前用户关注的up主
        Integer total = userService.getCountByNick(nick);
        page.setTotal(total);
        if (total>0){
            List<UserInfo> userInfoList = userService.getUserInfoByPageRecords(params);
            page.setList(userInfoList);
        }

        return new JsonResponse<>(page);
    }


}

package com.lizejin.bilibili.api;


import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.JsonResponse;
import com.lizejin.bilibili.domain.auth.UserAuthorities;
import com.lizejin.bilibili.service.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserAuthApi {

    @Autowired
   private UserSupport userSupport;
    @Autowired
   private UserAuthService userAuthService;

    //对当前用户前端可见元素与页面，根据不同角色进行不同的限制
    @GetMapping("/user-authorities")
    public JsonResponse<UserAuthorities> getUserAuthorities(){
        Long userId = userSupport.getCurrentUserId();
        UserAuthorities userAuthorities = userAuthService.getUserAuthorities(userId);
        return new JsonResponse<>(userAuthorities);
    }
}

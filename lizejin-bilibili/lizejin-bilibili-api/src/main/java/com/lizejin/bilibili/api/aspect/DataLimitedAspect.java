package com.lizejin.bilibili.api.aspect;

import com.lizejin.bilibili.api.support.UserSupport;
import com.lizejin.bilibili.domain.FollowingGroup;
import com.lizejin.bilibili.domain.auth.UserRole;
import com.lizejin.bilibili.domain.constant.AuthRoleConstant;
import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.UserRoleService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Order(1)
@Component
@Aspect
public class DataLimitedAspect {
    @Autowired
    private UserSupport userSupport;
    @Autowired
    private UserRoleService userRoleService;

    @Pointcut("@annotation(com.lizejin.bilibili.domain.annotation.DataLimited)")
    public void check(){}

    @Before("check()")
    public void doBefore(JoinPoint joinPoint){
        Long userId = userSupport.getCurrentUserId();
        List<UserRole> userRole = userRoleService.getUserRoleByUserId(userId);
        Set<String> collect = userRole.stream().map(UserRole::getRoleCode).collect(Collectors.toSet());
        Object[] args = joinPoint.getArgs();
        for(Object arg: args){
            if(arg instanceof FollowingGroup){
                String name = ((FollowingGroup) arg).getName();
                if(collect.contains(AuthRoleConstant.ROLE_LV0) || name == "精彩关注" ){
                    throw new ConditionException("Lvo等级是无法进行<<"+name+">>的创建");
                }
            }

        }
    }
}

package com.lizejin.bilibili.api;

import com.lizejin.bilibili.domain.JsonResponse;
import com.lizejin.bilibili.service.DaoService;
import com.lizejin.bilibili.service.utils.FastDFSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping
public class DemoApi {

    @Autowired
    private DaoService daoService;
    @Autowired
    private FastDFSUtil fastDFSUtil;

    @GetMapping("/demo")
    public void getName(int id){
        System.out.println("..........."+daoService.getName(id)+"..........");
    }

    @PutMapping("/slices")
    public void uploadSliceFile(MultipartFile file) throws IOException {
       fastDFSUtil.ConvertFileToSlice(file);
    }
}

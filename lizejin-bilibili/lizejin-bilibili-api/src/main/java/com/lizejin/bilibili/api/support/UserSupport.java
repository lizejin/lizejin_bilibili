package com.lizejin.bilibili.api.support;

import com.lizejin.bilibili.domain.exception.ConditionException;
import com.lizejin.bilibili.service.utils.TokenUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class UserSupport {

    public Long getCurrentUserId(){
        //抓取请求
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取token并验证token
        String token = requestAttributes.getRequest().getHeader("token");
        Long userId = TokenUtils.verifyToken(token);
        if (userId<0){
            throw new ConditionException("非法用户！");
        }
        return userId;
    }
}

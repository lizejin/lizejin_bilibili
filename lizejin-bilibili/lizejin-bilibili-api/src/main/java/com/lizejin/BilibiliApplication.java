package com.lizejin;


import com.lizejin.bilibili.service.websocket.WebSocketService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableAsync
@EnableScheduling
public class BilibiliApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(BilibiliApplication.class,args);
        WebSocketService.setApplicationContext(applicationContext);//在这里将上下文对象传给websocket服务端
        System.out.println("bilibili项目已经启动");
    }
}
